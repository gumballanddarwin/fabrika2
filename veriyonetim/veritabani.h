#ifndef VERITABANI_H
#define VERITABANI_H

#include<QObject>
#include <C:/Fabrika/Veriyonetim/calisanveriyoneticisi.h>
#include <C:/Fabrika/Veriyonetim/uretimveriyoneticisi.h>
#include <C:/Fabrika/Veriyonetim/urunveriyonetimi.h>
#include <C:/Fabrika/Veriyonetim/vardiyaveriyoneticisi.h>
#include <Veri_global.h>


class VeriTabani : public QObject
{
    Q_OBJECT
  private:
    explicit VeriTabani(QObject *parent = nullptr);
public:
    static VeriTabani &veritabani();

   CalisanVeriYoneticisi &calisan();

    UretimVeriYoneticisi &uretim();

    UrunVeriYonetimi &urun();

    VardiyaVeriYoneticisi &vardiya();

private :
    CalisanVeriYoneticisi _calisan;
    UretimVeriYoneticisi _uretim;
    UrunVeriYonetimi _vardiya;


signals:
void urunEklendi(urun::Ptr urun);
void urunSilindi(urun::Ptr urun);
};

#endif // VERITABANI_H
