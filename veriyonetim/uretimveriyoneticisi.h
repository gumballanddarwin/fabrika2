#ifndef URETIMVERIYONETICISI_H
#define URETIMVERIYONETICISI_H

#include <QObject>
#include"temelveriyonetimsinifi.h"
#include<C:/Fabrika/Verisiniflari/uretim.h>
#include<Veri_global.h>
typedef typename Pointer;

class VERI_EXPORT UretimVeriYoneticisi : public QObject,public temelveriyonetimsinifi<uretim>
{
    Q_OBJECT
public:
    explicit UretimVeriYoneticisi(QObject *parent = nullptr);

signals:
    void elemanEklendi(Pointer ptr);
    void elemanSilindi(Pointer ptr);
    void elemanDegisti(Pointer eski,Pointer yeni);

};

#endif // URETMVERIYONETICISI_H
