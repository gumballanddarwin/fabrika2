#ifndef CALISANVERIYONETICISI_H
#define CALISANVERIYONETİCİSİ_H

#include<QObject>
#include "temelveriyonetimsinifi.h"
#include<verisiniflari/calisan.h"
#include<Veri_global.h>

class VERI_EXPORT CalisanVeriYoneticisi : public QObject,
                   public TemelVeriYonetimsinifi<Calisan>{
    Q_OBJECT
    public:
        explicit CalisanVeriYoneticisi(QObject *parent = nullptr);
signals:
    void elemanEklendi(Pointer ptr);
    void elemanSilindi(Pointer ptr);
    void elemanDegisti(Pointer eski,Pointer yeni);

};
#endif // CALISANVERIYONETİCİSİ_H
