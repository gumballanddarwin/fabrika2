#include "veritabani.h"


VeriTabani::VeriTabani(QObject *parent) : QObject(parent)
{connect(&_urun, &urunveriyonetimi::elemanEklendi,this,
         &VeriTabani::urunEklendi);

}

VeriTabani &VeriTabani::veritabani() {

    static VeriTabani nesne;
    return nesne;
}

CalisanVeriYoneticisi &VeriTabani::calisan() {
    return _calisan;
}

UretimVeriYoneticisi &VeriTabani::uretim() {
    return _uretim;
}
UrunVeriYonetimi &VeriTabani::urun(){
    return _urun;
}

VardiyaVeriYoneticisi &VeriTabani::vardiya()
{
    return _vardiya;
}
