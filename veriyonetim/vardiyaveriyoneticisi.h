#ifndef VARDIYAVERIYONETICISI_H
#define VARDIYAVERIYONETICISI_H


#include <QObject>

#include"temelveriyonetimsinifi.h"

#include<C:/Fabrika/Verisiniflari/vardiya.h>
#include<Veri_global.h>
typedef typename Pointer;

class VERI_EXPORT VardiyaVeriYoneticisi : public QObject,
       public temelveriyonetimsinifi<vardiya>
{
    Q_OBJECT
public:
    explicit VardiyaVeriYoneticisi(QObject *parent = nullptr);

signals:
void elemanEklendi(Pointer ptr);
void elemanSilindi(Pointer ptr);
void elemanDegisti(Pointer eski,Pointer yeni);
};

#endif // VARDIYAVERIYONETICISI_H
