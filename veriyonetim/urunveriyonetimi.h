#ifndef URUNVERIYONETIMI_H
#define URUNVERIYONETIMI_H

#include <QObject>
#include"temelveriyonetimsinifi.h"
#include<C:/Fabrika/Verisiniflari/urun.h>
#include<Veri_global.h>

typedef typename Pointer;
class VERI_EXPORT UrunVeriYonetimi : public QObject,public temelveriyonetimsinifi<urun>
{
    Q_OBJECT
public:
    explicit UrunVeriYonetimi(QObject *parent = nullptr);

signals:
    void elemanEklendi(Pointer ptr);
    void elemanSilindi(Pointer ptr);
    void elemanDegisti(Pointer eski,Pointer yeni);


};

#endif // URUNVERIYONETIMI_H

