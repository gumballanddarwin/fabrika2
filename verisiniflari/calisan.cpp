#include "calisan.h"

Calisan::Calisan(QObject *parent) :temelverisinifi(parent)
{ _calisanid =0;
  _tckimlik ="";
  _adi ="";
  _soyadi ="";
  _adres ="";
  _sicilno ="";

}

Calisan::IdTuru Calisan::calisanid() const {
    return _calisanid;

}
void Calisan::setCalisanid(const IdTuru &calisanid) {
    if( _calisanid !=calisanid) {
        _calisanid = calisanid;
        calisanidDegisti(calisanid);}
}

Calisan::Metin Calisan::tckimlik() const {
    return _tckimlik;
}
void Calisan::setTckimlik(const QString &tckimlik){
    if( _tckimlik != tckimlik){
        _tckimlik =tckimlik;
        tckimlikDegisti(_tckimlik);
    }
}
Calisan::Metin Calisan::adi() const {
    return _adi;
}
void Calisan::setAdi(const QString &adi) {
    if(_adi!= adi){
        _adi =adi;
        adiDegisti(_adi);
    }
}

Calisan::Metin Calisan::soyadi() const {
    return _soyadi;
}
void Calisan::setSoyadi(const QString &soyadi) {
    if(_soyadi !=soyadi) {
        _soyadi = soyadi;
        soyadiDegisti(_soyadi);

    }
}
Calisan::Metin Calisan::adres()const {
    return  _adres;
}
void Calisan::setAdres(const QString &adres)
{ if(_adres !=adres){
        _adres= adres;
        adresDegisti(_adres);
    }
}

Calisan::Metin Calisan::sicilno() const {
    return _sicilno;

}

void Calisan::setSicilno(const QString &sicilno)  {
    if(_sicilno !=sicilno){
        _sicilno = sicilno;
        sicilnoDegisti(_sicilno);
    }
}



