#ifndef URETIM_H
#define URETIM_H

#include <QObject>


#include <QObject>
#include <memory>
#include "temelverisinifi.h"
#include <Veri_global.h>

using namespace std;


class VERI_EXPORT uretim : public temelverisinifi
{
    Q_OBJECT
public:
    typedef uretim Veri;
    typedef shared_ptr<Veri> Ptr;


public:
    explicit uretim(QObject *parent = nullptr);

    IdTuru uretimid() const;
    void setUretimid (const IdTuru &uretmid);

    IdTuru vardiyaid() const ;
    void setVardiyaid(const IdTuru &vardiyaid);

    IdTuru calisanid() const;
    void setCalisanid(const IdTuru &calisanid);

    IdTuru urunid() const;
    void setUrun(const IdTuru &urunid);

    IsaretsizTamsayı miktar() const;
    void setMiktar(const IsaretsizTamsayı &miktar);

    inline IsaretsizTamsayı toplamMiktar(){return _urunid *_miktar;}
    static Ptr yeni() {return std::make_shared<Veri>();}


    Ptr kopyaOlustur() {
        auto kopya=yeni();
        kopya->_uretimid=this->_uretimid;
        kopya->_vardiyaid=this->_vardiyaid;
        kopya->_calisanid=this->_calisanid;
        kopya->_urunid=this->_urunid;
        kopya->_miktar=this->_miktar;
        return kopya;
    }

signals:
    void uretimidDegisti(const IdTuru &uretimid);
    void vardiyaidDegisti(const IdTuru &vardiyaid);
    void calisanidDegisti(const IdTuru &calisanid);
    void urunidDegisti(const IdTuru &urunid);
    void miktarDegisti(const IsaretsizTamsayı &miktar);
    void toplammiktarDegisti(const IsaretsizTamsayı &toplammiktar);
private:
    IdTuru _uretimid;
    IdTuru _vardiyaid;
    IdTuru _calisanid;
    IdTuru _urunid;
   IsaretsizTamsayı _miktar;


};

#endif // URETIM_H

