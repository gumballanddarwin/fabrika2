#ifndef URUN_H
#define URUN_H

#include <QObject>
#include<QString>
#include<memory>
#include "temelverisinifi.h"
#include <Veri_global.h>

using namespace std;


class VERI_EXPORT urun : public temelverisinifi

{
    Q_OBJECT
public:
    typedef urun Veri;
    typedef shared_ptr<Veri> Ptr;
public:
    explicit urun(QObject *parent = nullptr);

    IdTuru urunid() const;
    void setUrunid(const IdTuru &urunid);

    Metin urunadi() const;
    void setUrunadi(const Metin &urunadi);
    static Ptr yeni() {return std ::make_shared<Veri>();}

    Ptr kopyaOlustur(){
        auto kopya=yeni();
        kopya->_urunid=this->_urunid;
        kopya->_urunadi =this->_urunadi;
        return kopya;

    }

signals:
void urunidDegisti(const IdTuru &urunid);
void urunadiDegisti(const Metin &urunadi);


private:
IdTuru _urunid;
Metin _urunadi;
};



#endif // URUN_H
