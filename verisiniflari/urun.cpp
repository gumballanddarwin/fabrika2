#include "urun.h"

urun::urun(QObject *parent) : temelverisinifi(parent)

{ _urunid =0;
_urunadi="";
}

urun::IdTuru urun::urunid() const
{
    return _urunid;
}

void urun ::setUrunid(const IdTuru &urunid){
    if(_urunid != urunid){
        _urunid=urunid;
        urunidDegisti(_urunid);
    }
}

urun::Metin urun::urunadi() const
{
    return _urunadi;
}

void urun::setUrunadi(const Metin &urunadi){
    if(_urunadi !=urunadi){
        _urunadi=urunadi;
        urunadiDegisti(_urunadi);
    }
}

