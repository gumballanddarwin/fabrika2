#ifndef TEMELVERISNIFI_H
#define TEMELVERISINIFI_H

#include <QObject>
#include<QString>
#include<memory.h>
#include<QDateTime>


class temelverisinifi : public QObject
{
    Q_OBJECT
public:
    typedef QString Metin;
    typedef unsigned long long IdTuru;
    typedef int Tamsayi;
    typedef unsigned int IsaretsizTamsayı;
    typedef QDateTime TarihSaat;

public:
   explicit temelverisinifi (QObject *parent = nullptr);

    IdTuru id() const;
    void setId(const IdTuru &id);

signals:
    void idDegisti(const IdTuru &id);
protected:
    IdTuru _id;


};

#endif // TEMELVERISINIFI_H
