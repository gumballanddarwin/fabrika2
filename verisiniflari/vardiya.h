#ifndef VARDIYA_H
#define VARDIYA_H

#include <QObject>
#include<QDateTime>
#include<memory>
#include"temelverisinifi.h"
#include<Veri_global.h>

using namespace std;


class VERI_EXPORT vardiya : public temelverisinifi
{
    Q_OBJECT
public:
    typedef vardiya Veri;
    typedef shared_ptr<vardiya> Ptr;

public:
    explicit vardiya(QObject *parent = nullptr);


    IdTuru vardiyaid() const;
    void setVardiyaid(const IdTuru &vardiyaid);

    TarihSaat vardiyabaslangıc() const;
    void setVardiyabaslangic(const TarihSaat &vardiyabaslangıc);

    QDateTime vardiyabitis() const;
    void setVardiyabitis(const TarihSaat &vardiyabitis);
    static Ptr yeni() {return std::make_shared<Veri>();}

    Ptr kopyaOlustur(){
        auto kopya=yeni();
        kopya->_vardiyaid=this->_vardiyaid;
        kopya->_vardiyabaslangic=this->_vardiyabaslangic;
        kopya->_vardiyabitis=this->_vardiyabitis;
        return kopya;
    }
signals:
    void vardiyaidDegisti(const IdTuru &vardiyaid);
    void vardiyabaslangıcDegisti(const TarihSaat &vardiyabaslangic);
    void vardiyabitisDegisti(const TarihSaat &vardiyabitis);

private:
    IdTuru _vardiyaid;
    TarihSaat _vardiyabaslangic;
    TarihSaat _vardiyabitis;

};

#endif // VARDIYA_H

