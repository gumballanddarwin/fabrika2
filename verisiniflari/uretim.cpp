
#include "uretim.h"

uretim::uretim(QObject *parent) : temelverisinifi(parent)
{
_uretimid = 0;
_calisanid =0;
_urunid= 0;
_miktar =0.0f;

}

uretim::IdTuru uretim::uretimid() const {
    return _uretimid;
}

void uretim::setUretimid(const IdTuru &uretimid){
    if(_uretimid != uretimid){
        _uretimid =uretimid;
        uretimidDegisti(uretimid);}
}

uretim::IdTuru uretim::vardiyaid()const{
    return _vardiyaid;
}

void uretim::setVardiyaid(const IdTuru &vardiyaid){
    if(_vardiyaid !=vardiyaid){
        _vardiyaid =vardiyaid;
        vardiyaidDegisti(vardiyaid);
    }
}
uretim::IdTuru uretim::calisanid()const{
    return _calisanid;
}
void uretim::setCalisanid(const IdTuru &calisanid){
       if(_calisanid != calisanid){
           _calisanid = calisanid;
           calisanidDegisti(calisanid);}
       }
uretim::IdTuru uretim::urunid() const
{
    return _urunid;
}
void uretim::setUrun(const IdTuru &urunid)
{ if(_urunid != urunid){
        _urunid = urunid;
        urunidDegisti(urunid);}
    }
uretim::IsaretsizTamsayı uretim::miktar() const
{
    return _miktar;
}

void uretim::setMiktar(const IsaretsizTamsayı &miktar)
{ if(_miktar !=miktar){
        _miktar =miktar;
        miktarDegisti(miktar);}
    }
