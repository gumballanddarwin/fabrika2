#ifndef CALISAN_H
#define CALISAN_H

#include <QObject>
#include<QString>
#include<memory>

#include"temelverisinifi.h"
#include <Veri_global.h>

using namespace std;


class VERI_EXPORT Calisan : public temelverisinifi
{
    Q_OBJECT
public:
    typedef Calisan Veri;
    typedef shared_ptr<Veri> Ptr;
public :
    explicit Calisan(QObject*parent =nullptr);

    IdTuru calisanid ()const;
    void setCalisanid(const IdTuru &calisanid);

    QString tckimlik() const;
    void setTckimlik(const QString &tckimlik);

   QString adi() const;
    void setAdi(const QString &adi);

    QString soyadi() const ;
    void setSoyadi(const QString &soyadi);

    QString adres() const ;
    void setAdres(const QString &adres);

    QString sicilno() const;
    void setSicilno (const QString &sicilno);

    static Ptr yeni() {return std ::make_shared <Veri>();}

    Ptr kopyaOluştur() {
        auto kopya =yeni();
        kopya->_calisanid =this->_calisanid;
        kopya->_tckimlik =this->_tckimlik;
        kopya->_adi =this->_adi;
        kopya->_soyadi =this->_soyadi;
        kopya->_adres =this->_adres;
        kopya->_sicilno =this->_sicilno;

        return kopya;
    }


signals:
    void calisanidDegisti(const IdTuru &calisanid);
    void tckimlikDegisti(const Metin &tckimlik);
    void adiDegisti(const Metin &adi);
    void soyadiDegisti(const Metin &soyadi);
    void adresDegisti(const Metin &adres);
    void sicilnoDegisti(const Metin &sicilno);

private :
    IdTuru _calisanid;
    Metin _tckimlik;
    Metin _adi;
    Metin _soyadi;
    Metin _adres;
    Metin _sicilno;




};

#endif // CALISAN_H
